# EPN2EOSMonitor

EPN2EOSMonitor is a MonaLisa client that subscribes to MonaLisa and receives
predicates about the running EPN2EOS transfers from the EPNs to the EOSALICEO2.

## Project structure

The project is made up of the `java-interface`, which containes the Java code
used for the application and a `Dockefile` used for building and running the
tool inside of a container.

The `test-env` container is used for testing the solution using a complete
software stack, running an InfluxDB database and a Grafana server for
visualizing the resulting output.

## Building

The repo is packaged using Maveon, do in order to output the JAR file used for
running the monitor you must use the following command:

```
sergiu@epsilon:~/build/epn2eosmonitor$ cd java-interface/
sergiu@epsilon:~/build/epn2eosmonitor/java-interface$ mvn package
```

## Running

The setup can be run directly on the machine using the `./java-interface/run.sh` script which starts the EPN2EOSMonitor.

## Config files

The only config file do be used is the
`./java-interface/conf/influxdb.properties` file, which can inherit connection
and authentication information for the database as follows:

```
influxdb.url = http://localhost:8086
influxdb.user = root
influxdb.password = root
influxdb.database = epn2eosruns
```

You can skip the username and password configuration and the client will try
connecting to the database without authentication.
