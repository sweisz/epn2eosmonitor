#!/bin/bash

#
# run_client.sh clusters
#
# example:
#   run_client.sh "HPCS:LBL:ORNL"
#

export JAVA_HOME=`cat conf/env.JAVA_HOME`
export PATH=$JAVA_HOME/bin:$PATH
#CP="bin"
for a in ./target/dependency/*.jar; do
    CP="$CP:$a"
done
CP="./target/EPN2EOSMonitor-1.0.0.jar:$CP"

pwd
mkdir log 2> /dev/null
tag=`date +%Y.%m.%d-%H:%M:%S`
mv log/EPN2EOSMonitor log/EPN2EOSMonitor-$tag 2> /dev/null
mv log/app log/app-$tag 2> /dev/null

echo "running EPN2EOSMonitor ..."
echo ;

echo "logs are in \"log\" directory"
echo ;

ls -l ./conf/influxdb.properties
java \
    -server -Xmx256m -XX:CompileThreshold=500 \
    -classpath ${CP} \
    -Djava.security.policy=conf/policy.all \
    -Dlia.Monitor.ConfigURL=file:conf/App.properties \
    -Djava.util.logging.config.class=lia.Monitor.monitor.LoggerConfigClass \
    EPN2EOSMonitor >> log/EPN2EOSMonitor 2>&1

echo ;
