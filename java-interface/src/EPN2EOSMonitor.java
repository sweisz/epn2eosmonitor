import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import lia.Monitor.JiniClient.Store.Main;
import lia.Monitor.monitor.AccountingResult;
import lia.Monitor.monitor.DataReceiver;
import lia.Monitor.monitor.ExtResult;
import lia.Monitor.monitor.MFarm;
import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.ShutdownReceiver;
import lia.Monitor.monitor.eResult;
import lia.Monitor.monitor.monPredicate;

public class EPN2EOSMonitor {
	private static final Logger logger = Logger.getLogger(EPN2EOSMonitor.class.getName());
	public static InfluxDBClient influxDB = null;
	public static WriteApiBlocking writeApi = null;

	public static void main(String[] args){
		String url = null;
		String org = null;
		String bucket = null;
		String token = null;
		try (InputStream input = new FileInputStream("./conf/influxdb.properties")) {

			Properties prop = new Properties();
			prop.load(input);

			// get the property value and print it out
			url = prop.getProperty("influxdb.url");
			org = prop.getProperty("influxdb.org");
			bucket = prop.getProperty("influxdb.bucket");
			token = prop.getProperty("influxdb.token");

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		logger.log(Level.WARNING, url);
		logger.log(Level.WARNING, org);
		logger.log(Level.WARNING, bucket);
		logger.log(Level.WARNING, token);

		if (url == null)
			url = "http://localhost:8086";

		if (org == null)
			influxDB = InfluxDBClientFactory.create(url);
		else
			influxDB = InfluxDBClientFactory.create(url, token.toCharArray(), org, bucket);

		if (influxDB == null) {
			logger.log(Level.INFO, "Could not init DB");
			return;
		}

		writeApi = influxDB.getWriteApiBlocking();

		try {
			FileHandler fh;
			// This block configure the logger with handler and formatter
			fh = new FileHandler("log/app");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			logger.setUseParentHandlers(false);
			logger.setLevel(Level.FINEST);
			logger.log(Level.INFO, "finished logging");

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		logger.log(Level.INFO, "App is starting");
		logger.log(Level.WARNING, influxDB.toString());
		logger.log(Level.WARNING, "Version " + influxDB.version());

		// start the repository service
		final Main jClient = new Main();
		logger.log(Level.WARNING, "Started client");

		// register a MyDataReceiver object to receive any new information.
		jClient.addDataReceiver(new MyDataReceiver());
		logger.log(Level.WARNING, "Receiver added successfully");

		boolean ret;

		String farm = "alicdb3.cern.ch";

		logger.log(Level.WARNING, "Connected to farm " + farm);
		ret = jClient.registerPredicate(new monPredicate(farm, "ALIEN_epn2eos-transient-data_Nodes", "*", -1, -1,
					new String[]{"*"}, null));
		if (!ret)
			logger.log(Level.WARNING, "predicate for " + farm + " not registered");
	}

	/**
	 * This is a very simple data receiver that puts some filters on the received data
	 * and outputs the matching values on the console.
	 */
	private static class MyDataReceiver implements DataReceiver, ShutdownReceiver {

		MyDataReceiver() {
			logger.log(Level.FINEST, "In Receiver contructor");
		}

		public void Shutdown(){
			System.out.flush();
		}

		public void addResult(eResult r) {
			logger.log(Level.FINEST, r.toString());
		}

		//time=$val Farm=$val Node=$val $param_name=$param ...
		private void buildMessageEos(Result r) {


			for (int i = 0; i < r.param.length; i++) {
				if (r.param_name[i].contains("tf_R") || r.param_name[i].contains("root_R")) {
					Point p = Point.measurement("epn2eos")
						.time(r.getTime(), WritePrecision.MS)
						.addTag("hostname", r.NodeName)
						.addTag("system", "EOS");

					String runNr = r.param_name[i].split("-")[0];
					String other = r.param_name[i].split("-")[1];
					String type = other.split("_")[0];

					p.addTag("runNumber", runNr);
					p.addField("fileType", type);
					p.addField("bytesPerSecond", r.param[i]);

					logger.log(Level.FINEST, p.toString());

					writeApi.writePoint(p);
				}
			}

		}

		/*      We received a new entry
		 * */
		public void addResult(Result r){
			logger.log(Level.FINEST, r.toString());
			if (r.ClusterName.equals("ALIEN_epn2eos-transient-data_Nodes")) {
				buildMessageEos(r);
			}

		}

		public void addResult(ExtResult er){
			logger.log(Level.FINEST, er.toString());
		}

		public void addResult(AccountingResult ar){
			logger.log(Level.FINEST, ar.toString());
		}

		public void updateConfig(MFarm f){}
	}
}
