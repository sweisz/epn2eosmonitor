#!/bin/bash
# do not run this with sudo, because it would create dirs with root owner and we do not want this
# also this why we have to create these directories manually, because docker-compose would create them, but with root

#mkdir grafana/ influxdb/
#cd -

# https://github.com/bitnami/bitnami-docker-influxdb
# https://docs.influxdata.com/influxdb/v1.8/administration/authentication_and_authorization/#authorization

# Build and run container
#docker-compose up -d --build db

# Initialase database - modifications are persistent because we mounted db path in docker-compose.yml
# - $VOLUMES_DIR/influxdb:/var/lib/influxdb
ADMIN_TOKEN=nqZNW3J1TnXLAX_aQPsKLkoNXfOaPndzcbOUPbWvFmWRlqssQ4tf-a8_pNh9bZj1NUQMu_fNWu3zPuDrWURcWA==
#docker-compose run \
#      -e DOCKER_INFLUXDB_INIT_USERNAME=root \
#      -e DOCKER_INFLUXDB_INIT_PASSWORD=rootpassword \
#      -e DOCKER_INFLUXDB_INIT_ORG=cern \
#      -e DOCKER_INFLUXDB_INIT_BUCKET=eos \
#      -e DOCKER_INFLUXDB_INIT_ADMIN_TOKEN="$ADMIN_TOKEN" \
#      -v ./influxdb:/var/lib/influxdb \
#      db

#docker exec  influxdb-db influx -execute 'bucket list'
docker exec influxdb-db influx setup -u root -p rootpassword --org cern -t "$ADMIN_TOKEN" --bucket eos --retention 0 -f
docker exec influxdb-db influx query -t $ADMIN_TOKEN --org cern "list buckets"
